extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var rd = false
# Called when the node enters the scene tree for the first time.
func _ready():
	rd = true
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func trigger(which):
	if rd :
		print(which)
		if which == 0:
			if !$BulletSound.playing :
				$BulletSound.play()
		if which == 1:
			if !$BulletSound2.playing :
				$BulletSound2.play()
		if which == 2:
			if !$BulletSound3.playing :
				$BulletSound3.play()
