extends KinematicBody2D

export var moveSpeed := 240 # visible et modifiable dans l'inspecteur

onready var raycast = $Visor/RayCast2D
var joy = -1

var power = 0
var target = null


func _ready():
  set_meta("type", "player")
  yield(get_tree(), "idle_frame")


func target():
  if raycast.is_colliding():
    var coll = raycast.get_collider()
    if coll.get_meta("type") == "enemy" :
      target = coll
  #		print(target)
      $Select.global_position = target.global_position
      $Select.visible = true
  else :
    target = self
    $Select.visible = false






func _physics_process(delta):
  target()
  
  #$Area2D.global_rotation=-get_parent().global_rotation
  $Area2D/Sprite.rotate(delta*0.5)
  $Area2D/Sprite2.rotate(delta*-0.2)
  var move_vec := Vector2()
  var shift = Input.is_action_pressed("cruise")
  if Input.is_action_pressed("move_up"):
    move_vec.y -= 1
  if Input.is_action_pressed("move_down"):
    move_vec.y += 1
  if Input.is_action_pressed("move_left"):
    move_vec.x -= 1
    $PlayerSprite.flipped = true
  if Input.is_action_pressed("move_right"):
    move_vec.x += 1
    $PlayerSprite.flipped = false
  move_vec = move_vec.normalized()
  var mov = moveSpeed
  
  if shift :
    print ("adopting cruise control")
    mov = 50
  
  var final_move_vec: Vector2 = move_vec * mov * delta
  move_and_collide(final_move_vec)
  $PlayerSprite.playerSpeed = final_move_vec.length()
  
  if joy != -1 :
    var ax0 = Input.get_joy_axis(joy, JOY_AXIS_0)
    var ax1 = Input.get_joy_axis(joy, JOY_AXIS_1)
    var ax3 = Input.get_joy_axis(joy, JOY_AXIS_2)
    var ax4 = Input.get_joy_axis(joy, JOY_AXIS_3)
    if !(ax4 == 0) and !(ax3 == 0) :
      $Visor.global_rotation = atan2(ax3,-ax4)+2.35619/3
  else :
    var lookvec = get_global_mouse_position()-global_position
    var rot = atan2(lookvec.y,lookvec.x)
    $Visor.global_rotation=rot+2.35619
    
    
  if Input.is_action_just_pressed("shoot"):
    var coll = raycast.get_collider()
    if raycast.is_colliding() and coll.has_method("kill"):
      coll.kill()

func kill():
  get_tree().reload_current_scene()

func _on_Area2D_body_entered(body):
  if body.get_meta("type")=="enemy":
    print("")
    body.control=true
    raycast.add_exception(body)




func _on_Area2D_body_exited(body):
  if body.get_meta("type")=="enemy":
    print("bye")
    body.control=false
    raycast.remove_exception(body)
  pass # Replace with function body.
  
  
  
func _on_joy_connection_changed(device_id, connected):
  if connected:
    print(Input.get_joy_name(device_id))
    joy = device_id
  else:
    print("Keyboard")
    joy = -1
    
    
