extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(Texture) var weaponSprite

var playback: AnimationNodeStateMachinePlayback

# Called when the node enters the scene tree for the first time.
func _ready():
	playback = $AnimationTree.get("parameters/playback")
	$AnimationTree.active = true
	$SpriteTransform/SpriteWeapon.texture = weaponSprite

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

export(float) var threshold = 0.05

var golemSpeed = 0 setget _setGolemSpeed, _golemSpeed
var flipped = false setget _setFlipped, _flipped

func _setGolemSpeed(val):
	golemSpeed = val
	if golemSpeed > threshold:
		playback.travel("run")
	else:
		playback.travel("idle")

func _golemSpeed():
	return golemSpeed

func _setFlipped(val):
	$SpriteTransform/AnimatedSprite.flip_h = val
	$SpriteTransform/SpriteWeapon.flip_h = val

func _flipped():
	return $AnimatedSprite.flip_h
