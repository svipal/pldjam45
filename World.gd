extends Node2D

# Declare member variables here. Examples:
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():



  for i in range(0,6):
    spawnLEnemy(40+i*21,50+i*311,-1,1,70,1340)

  
  for i in range(0,5):
    spawnEnemy(140+i*221,150+i*103,1,0.3,20,60)

#
#	for i in range(0,5):
#		spawnEnemy(240+i*2,250+i*3,2,2,30,680)

#	for i in range(0,5):
#		spawnZombie(440+i*2,450+i*3,-1)

func spawnEnemy(x,y,cS,interval,speed,bSpeed):
  
  var _zombie = preload("res://Enemy.tscn").instance()
  _zombie.name="Enemy"
  _zombie.position=Vector2(x,y)
  _zombie.prepare($Player,cS, interval, speed, bSpeed )
  add_child(_zombie)


func spawnLEnemy(x,y,cS,chargeSpeed,speed,lSpeed):
  
  var _zombie = preload("res://LaserEnemy.tscn").instance()
  _zombie.name="LEnemy"
  _zombie.position=Vector2(x,y)
  _zombie.prepare($Player,cS, chargeSpeed, speed,lSpeed)
  add_child(_zombie)




# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
