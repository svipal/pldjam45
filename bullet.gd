extends KinematicBody2D

const INFL_STRENGTH=0.3
var mvSpeed = 200



enum ControlScheme {
	SInfluence,
	SDirect,
	SInvert,
	SNone

}

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player = null
var vector 	= null
var makername 	= null
var control = false
var controlScheme = null
var target = null

func prepare(playerd, posX , posY,controlled, makerd,scheme, bspeed):
	mvSpeed = bspeed
	if playerd != null :
		player  = playerd
		var bPlayerPos = player.global_position
		global_position = Vector2(posX,posY)
		var destinationVec = bPlayerPos- global_position
		vector = destinationVec.normalized()
		control = controlled
		makername = makerd

		if scheme == 0:
			controlScheme = ControlScheme.SDirect
		elif scheme == 1:
			controlScheme = ControlScheme.SInfluence
		elif scheme == 2:
			controlScheme = ControlScheme.SInvert
		else :
			controlScheme = ControlScheme.SNone
			
		
			
		var color = Color(0.4,0.4,0.2)
		if control:
			if controlScheme == ControlScheme.SInvert :
				color = Color(0.1, 0.2, 0.8)
				vector = -vector
			elif controlScheme == ControlScheme.SDirect :
				var r  = player.raycast.global_rotation+2.35619+PI/2
				vector = Vector2(cos(r),sin(r))*2
				color  = Color(1, 0.2, 0.5)
			elif controlScheme == ControlScheme.SInfluence :
				var r  = player.raycast.global_rotation+2.35619+PI/2
				vector = Vector2(cos(r),sin(r))*2
				color = Color(0.4, 0.2, 0.9)
			elif controlScheme == ControlScheme.SNone :
				queue_free()
				
			$Sprite.modulate= color
		
	
	
func _physics_process(delta):
	if player == null:
		return
	var infvec = Vector2(0,0)
	if control and controlScheme == ControlScheme.SInfluence :
#		if target == null and player.target.get_meta("type") == ("enemy") :
		target = player.target
		if target != null and is_instance_valid(target):
			infvec = (target.global_position - global_position).normalized() 
		
	vector = (vector + infvec* INFL_STRENGTH)
	move_and_collide(vector * mvSpeed * delta)
	


# Called when the node enters the scene tree for the first time.
func _ready():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func kill():
	queue_free()


func _on_Timer_timeout():
	self.queue_free()
	pass # Replace with function body.



func _on_Area2D_body_entered(body):
	if vector == null:
		return
	if control :
		if body.get_meta("type")=="enemy" and !body.control:
			body.kill()
			self.kill()
	else :
		if body.name == "Player":
			body.kill()
			self.kill()