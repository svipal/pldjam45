extends Line2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"



var head
var target 
var laserSpeed = 1000
var stage = 0
var control = false
var player = null
var maker = null
var spawn_offset = Vector2(0, 0)

# Called when the node enters the scene tree for the first time.
func _ready():
	#$laserbase/AnimationPlayer.play("flicker")
	pass # Replace with function body.

func prepare(playerd, pX, pY, makerd, speed, spawn_offset):
	$RayCast2D.add_exception(makerd)
	player = playerd
	global_position = Vector2(pX,pY)
	laserSpeed = speed
	maker = makerd
	self.spawn_offset = spawn_offset
	
	if maker.control:
		$RayCast2D.collision_mask=2
	else :
		$RayCast2D.collision_mask=1



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	global_position = maker.global_position + self.spawn_offset
	if stage == 0:
		# target select
		head = global_position
		if !maker.control :
			target = player.global_position - global_position
		else :
			if player.target != null and is_instance_valid(player.target):
				target = player.target.global_position - global_position
			else :
				var r  = player.raycast.global_rotation+2.35619+PI/2
				var pvector = Vector2(cos(r),sin(r))*2000+ player.global_position
				target = pvector - global_position
		# t
		
		
	elif stage == 2:
		maker.isPreparing = true
		head = target.normalized()*laserSpeed
		if maker.control:
			head = head*1.5
		add_point(head)
	
	elif stage == 4:
		maker.isPreparing = false
		kill()
		
	if stage > 2:
		$RayCast2D.cast_to =Vector2(head.x,head.y)
		
	if $RayCast2D.is_colliding():
		var coll = $RayCast2D.get_collider()
		coll.kill()
	
			
	var r = atan2(target.x, -target.y) + PI/2
	$laserbase.rotation= r



func _on_Timer_timeout():
	stage +=1
	var ally = 0
	if maker.control :
		ally = 0.4
	if stage <= 2:
		modulate = Color(0.8+ally/4,0.8+ally,0.8-ally)
	elif stage == 3:
		modulate = Color(0.3,0.2,0.5)
	pass # Replace with function body.
	
	
func kill():
	 queue_free()

