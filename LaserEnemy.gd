extends KinematicBody2D

var mvSpeed = 70

onready var raycast = $RayCast2D

var player = null
var control = false
var enemy = true
var dead = false
var controlScheme = 0
var laserDelay = 2
var isPreparing = false
var laserSpeed = 1500




func _ready():
	set_meta("type", "enemy")
	

func _physics_process(delta):
	if player == null or dead:
		return
	
	if self.isPreparing:
		$GolemSprite.golemSpeed = 0
	elif control == false :
		var vec_to_player = player.global_position - global_position
		vec_to_player = vec_to_player.normalized()
#		global_rotation = atan2(vec_to_player.y, vec_to_player.x)
		var golem_mov_dir: Vector2 = vec_to_player * mvSpeed * delta
		move_and_collide(golem_mov_dir)
		$GolemSprite.golemSpeed = golem_mov_dir.length()
		$GolemSprite.flipped = vec_to_player.x < 0
		
		if raycast.is_colliding():
			var coll = raycast.get_collider()
			if coll.name == "Player":
				coll.kill()
	elif control == true :
		var vec_to_player = player.global_position - global_position
		$GolemSprite.flipped = vec_to_player.x < 0
		
		if vec_to_player.length() > 100 :
			vec_to_player = vec_to_player.normalized()
			var golem_mov_dir: Vector2 = vec_to_player * mvSpeed *1.5  * delta
			move_and_collide(golem_mov_dir)
			$GolemSprite.golemSpeed = golem_mov_dir.length()
		else:
			$GolemSprite.golemSpeed = 0
#		global_rotation = -atan2(vec_to_player.y, vec_to_player.x)
		
	if control == true:
		$GolemSprite.modulate= Color(0.5,0.7,0.7)
	else:
		$GolemSprite.modulate= Color(1,1,1)
		
func prepare(player,scheme,time,speed,lspeed):
	controlScheme = scheme
	$Timer.wait_time=time
	laserSpeed = lspeed
	mvSpeed = speed
	set_player(player)
		
func kill():
	dead= true
	$Explosion.trigger()
	collision_layer=32
	
	

func set_player(p):
	player = p

func _on_Timer_timeout():
	if dead:
		queue_free()
	else :
		$LaserSound.play()
		var _laser = preload("res://Laser.tscn").instance()
		get_parent().add_child(_laser)
		_laser.prepare(player, position.x, position.y, self, laserSpeed, Vector2(0, -40))
