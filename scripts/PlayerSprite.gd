extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var playback: AnimationNodeStateMachinePlayback

# Called when the node enters the scene tree for the first time.
func _ready():
	playback = $AnimationTree.get("parameters/playback")
	$AnimationTree.active = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

export(float) var threshold = 0.05

var playerSpeed = 0 setget _setPlayerSpeed, _playerSpeed
var flipped = false setget _setFlipped, _flipped

func _setPlayerSpeed(val):
	playerSpeed = val
	if playerSpeed > threshold:
		playback.travel("run")
	else:
		playback.travel("idle")

func _playerSpeed():
	return playerSpeed

func _setFlipped(val):
	$SpriteTransform/AnimatedSprite.flip_h = val

func _flipped():
	return $AnimatedSprite.flip_h
